﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExtensionMethodStringHelper
{
   public static class StringHelper
    {

        public static string ChangeFirstNameLetter(this string name)
        {
            if (name.Length > 0)
            {
                char[] array = name.ToCharArray();
                array[0] = char.IsUpper(array[0]) ? char.ToLower(array[0]) : char.ToUpper(array[0]);
                name = array.ToString();
                return new string(array);
            }

            return name;

        }


    }
}

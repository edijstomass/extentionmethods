﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ExtensionMethodStringHelper
{
    class Program
    {
        static void Main(string[] args)
        {

            string strName = "edijs";
            //string newString = StringHelper.ChangeFirstNameLetter(strName); // this is regular static method
            string newString = strName.ChangeFirstNameLetter();  // this is EXTENSION Method!!!
            Console.WriteLine(newString);

            // Just an example for extension method in built in .NET framework
            List<int> numbers = new List<int>() {1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            IEnumerable<int> evenNumbers = numbers.Where(n => n % 2 == 0); // extention method
            //IEnumerable<int> evenNumbers = Enumerable.Where(numbers, n => n % 2 == 0); // calling static method, both works the same

            foreach (int item in evenNumbers)
            {
                Console.WriteLine(item);
            }



        }
    }
}
